# This changes the branch only from master to main as it was renamed upstream
SRC_URI = "git://github.com/cracklib/cracklib;protocol=https;branch=main \
           file://0001-packlib.c-support-dictionary-byte-order-dependent.patch \
           file://0002-craklib-fix-testnum-and-teststr-failed.patch \
           "
