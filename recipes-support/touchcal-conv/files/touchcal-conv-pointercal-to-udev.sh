#!/bin/sh
#
# Allow 'local':
# shellcheck disable=SC3043

TOUCHDEVICE=""
INPUTFILE="/etc/pointercal"

# Event numbers (hex)
ABS_X=0
ABS_Y=1
ABS_MT_POSITION_X=35
ABS_MT_POSITION_Y=36


export VERBOSE="${VERBOSE:-0}"
if [ "$VERBOSE" -gt 0 ];then
    VERBOSE_PARAM="-v -v"
fi

# This function detects touch devices by checking their
# capabilities in the sysfs
is_touch_device()
{
    local DEVICE="${1}"
    local dev="${DEVICE##*/}"

    # is it a character device?
    [ -c "${DEVICE}" ] || return 1

    # Check if it is a touchscreen, assume ABS_X and ABS_Y
    # or ABS_MT_POSITION_X and ABS_MT_POSITION_Y are available axis
    DEV_SYSFS="/sys/class/input/$dev/device"

    # has it absolute events
    [ -r "${DEV_SYSFS}/capabilities/abs" ] ||  return 1
    CAP_ABS="$(cat "${DEV_SYSFS}/capabilities/abs")"
    CAP_ABS="${CAP_ABS%% *}" # remove eventual second part with spaces
    CAP_ABS="$( printf "%d" "0x${CAP_ABS}")"
    [ "$CAP_ABS" != "0" ] || return 1
    
    [ -r "$DEV_SYSFS/modalias" ] ||  return 1

    MODALIAS="$(cat "${DEV_SYSFS}/modalias")"
    MODALIAS="${MODALIAS##*-}" # Split up the generic part
    MODALIAS="${MODALIAS##*a}" # Split up everything before the absolute events

    if string_contains  "$MODALIAS" "${ABS_X},${ABS_Y},";
    then 
        return 0
    fi
    if string_contains  "$MODALIAS" ",${ABS_MT_POSITION_X},${ABS_MT_POSITION_Y},";
    then 
        return 0
    fi
}
#======================================================
# Argument parsing
#======================================================
usage()
{
    echo "$0 [--device=/dev/input/event0 ]"
    echo "Script to automatically convert the calibration in /etc/pointercal to be used in weston"
    echo "Parameters:"
    echo "    --device|-d: The device to assign the calibration to. If unset the an autodetection is done."   
    echo "    --inputfile|-i : The file to read the calibration from, default is /etc/pointercal"
}
# Does the string $1 contain the substring $2
string_contains()
{
    case "$1" in
        *$2*) return 0
    esac
    return 1
}

error_msg()
{
    echo "$*" >&2
}


# The function checks, if an argument is of the form
# key=value or key value, splits the string in the
# first case, and assigns the value to the retval
# (the first function parameter).
parse_args_value()
{
    retval="$1"
    p="$2"
    if string_contains "$p" "=";then
        v="${p##-*=}"
        r=0
    else
        v="$3"
        r=1
    fi
    if [ -z "$v" ];then
        error_msg "Parameter '$p' requires a non-empty option argument."
        exit 1
    fi
    eval "$retval"="$v"
    return $r
}

parse_args()
{
    while [ $# -ge 1 ]; do
        case $1 in
            -h|-\?|--help)   # Call a "usage" function to display a synopsis, then exit.
                usage
                exit 0
                ;;
            --device|--device=*|-d|-d=*)
                parse_args_value TOUCHDEVICE "$1" "$2" || shift
                ;;
            --inputfile|--inputfile=*|-i|-i=*)
                parse_args_value INPUTFILE "$1" "$2" || shift
                ;;
            --)            # End of all options.
                break
                ;;
            *)
                error_msg "Ignoring parameter $1"
                ;;
        esac
        shift
    done
}

parse_args "$@"



# create a udev rule setting the calibration for libinput base on /etc/pointercal
if [ ! -r "$INPUTFILE" ];
then
    echo "No touch calibration found at $INPUTFILE, skipped creating udev rule."
    exit 1
fi

if [ -z "$TOUCHDEVICE" ];
then
    # Loop through available input devices
    for dev in /dev/input/*
    do
        dev="${dev##*/}"
        DEVICE="/dev/input/$dev"
        if is_touch_device "$DEVICE";then
            TOUCHDEVICE="$DEVICE"
            break
        fi
    done
fi

if [ -z "$TOUCHDEVICE" ];
then
    echo "No touch device found, skipped creating udev rule."
    exit 1
fi

if [ ! -c /dev/fb0 ];
then
    echo "No framebuffer device found, skipped creating udev rule."
    exit 1
fi

# shellcheck disable=SC2086
/usr/bin/touchcal-conv to-udevrule --touchdevice="$TOUCHDEVICE" --inputfile "$INPUTFILE" $VERBOSE_PARAM
exit $?
