SUMMARY = "SECO Tool to convert touch calibration between tslib and libinput formats."
HOMEPAGE = "https://git.seco.com/clea-os/tools/seco-touchcal-conv"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRCREV = "${AUTOREV}"
SRC_URI = "git://git.seco.com/clea-os/tools/seco-touchcal-conv.git;protocol=https;branch=main;nobranch=1 \
            file://touchcal-conv-pointercal-to-udev.sh \
        "

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

inherit autotools

do_install:append() {
    install -d ${D}${libexecdir}
    install -m 0755 ${WORKDIR}/touchcal-conv-pointercal-to-udev.sh ${D}${libexecdir}/
}

FILES:${PN} += "${libexecdir}/*.sh"
