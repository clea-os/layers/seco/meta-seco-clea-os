FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://0001-Ignore-unkown-modules-in-ts.conf-print-warning-but-d.patch \
    file://ts_calibrate_extended.sh \
"

RDEPENDS:${PN}-calibrate += "seco-touchcal-conv"

do_install:append() {
    install -d ${D}${sysconfdir}/profile.d/available

    echo "alias ts_calibrate=\"${libexecdir}/ts_calibrate_extended.sh\"" > ${D}${sysconfdir}/profile.d/available/ts_calibrate.sh
    chmod 0755 ${D}${sysconfdir}/profile.d/available/ts_calibrate.sh

    ln -s available/ts_calibrate.sh ${D}${sysconfdir}/profile.d/ts_calibrate.sh

    install -d ${D}${libexecdir}
    install -m 0755 ${WORKDIR}/ts_calibrate_extended.sh ${D}${libexecdir}
}

FILES:tslib-calibrate += " \
    ${libexecdir}/ts_calibrate_extended \
    ${libexecdir}/ts_calibrate_extended.sh \
    ${sysconfdir}/profile.d/available/ts_calibrate.sh \
    ${sysconfdir}/profile.d/ts_calibrate.sh \
"
